# doto test

## telepítés:
  - eredtileg linuxon csináltam, de kipróbáltam a végén windows-on is sajna nem működött az "npm run init",   ami a server és a client csomagokat is feltepíteni egy lépésben

  - így külön le kell futtatni az npm install-t az alap és a client könyvtárban is
  
## megjegyzések:
  - telepítés után az "npm start" parancsal indul az app
  - a .env.example fájlból csináljatok egy .env másolatot a server innen szedi a környezeti változókat
  - kliensnél nem használtam .env-et ott be van égetve a localhost és a port is,
  (bár a port a client/package.json-ból jön), jobb lett volna ezt is a .env-ből szedni, de mindjárt lejár az időm, már nem akarom megvariálni
  - serveren a debug csomagot használtam, így az alkalmazás részei külön prefix-ekkel logolnak amit egyszerübb szűrni, illetve a .env-ben a DEBUG környezeti változóval is lehet szabályozni
  - server és kliens oldalon is nézi, hogy megfelelő paramétereket kap e router/ frontend app
  - egy input mezőben lehet megadni a méretet, mindíg a legelső cikkektől indul a lista ha átírjuk  a számot (5-10) közt
  - scroll-ra pedig hozzáadódnak a további elemek
