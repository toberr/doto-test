require('dotenv').config();

const express = require('express');
const path = require('path');
const fs = require('fs');
const debug = require('debug')('doto:app');
const cors = require('cors');

const router = require('./router/router');
const errorHandler = require('./error/errorHandler');

const PORT = process.env.PORT || 8000;
const HOST = process.env.HOST || 'localhost';
const isDev = (process.env.NODE_ENV == 'development');

const app = express();
const projectRoot = process.cwd();
process.title = 'doto';

function webApp() {
  
  function initCors() {
    app.use(cors({
      origin: function (origin, cb) {
        debug('origin', origin);
        if (/localhost|vbox/.test(origin) || !origin) {
          cb(null, true);
        } else {
          cb(new Error('Not allowed by CORS'));
        }
      }
    }));
  }
  
  function initRouter() {
    app.use(router);
  }
  
  function initStaticFiles() {
    app.use('/build', express.static(path.join(`${projectRoot}/client/build`)));
    if (isDev) {
      app.use('/build', express.static(path.join(`${projectRoot}/client/src`)));
    }
  }
  
  function initErrorHandler() {
    //error handler
    app.use(errorHandler);
  }
  
  function initApp() {
    app.listen(PORT, HOST, () => {
      debug(`${process.title} listening on http://${HOST}:${PORT}`);
      if (isDev) {
        fs.writeFileSync(`${process.cwd()}/client/src/.server-restart`, '');
      }
    });
  }
  
  initCors();
  initRouter();
  initStaticFiles();
  initErrorHandler();
  initApp();
  
  return app;
}

module.exports = webApp;



