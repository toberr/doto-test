const debug = require('debug')('doto:errorHandler');

function errorHandler(err, req, res, next) {
  debug(req.url);
  debug(err.stack);
  
  const errorStatus = err.status || 500;
  const errorMessage = err.toString() || 'Internal server error';
  let response = {
    errorMessage
  };
  
  response = process.env.NODE_ENV === 'development' 
    ? {...response, err: err} 
    : response;

  return res
    .status(errorStatus)
    .send(response);
}

module.exports = errorHandler;
