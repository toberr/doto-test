const express = require('express');
const router = express.Router();
const debug = require('debug')('doto:api');
const articleModel = require('../article-model');

router.use(express.json());

router.get('/:page/:size', (req, res, next) => {
  const { page, size } = req.params;
  const _page = Number(page);
  const _size = Number(size);
  if (_size >= 3 && _size <= 10 && _page > 0) {
    const result = articleModel(_page, _size);
    debug(page, size);
    debug('results', result);
    res.status(200).json(result);
  } else {
    res.status(400).json({
      msg: 'required format: "/api/page/size" the "size" parameter has to be a number between 3 and 10 and the "page" parameter has to be bigger then 0'
    });
  }
  
});

module.exports = router;
