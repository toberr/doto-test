const db = new Array(50).fill().map((x, i) => ({
  id: i+1,
  title: `title-${i+1}`,
  content: `content-${i+1}`
}));

function articleModel(page = 1, size = 10) {
  let groups = db.reduce((result, value, index, array) => {
    if (index % size * 1 === 0)
      result.push(db.slice(index, index + size * 1));
    return result;
  }, []);
  return groups[Number(page -1)] ? groups[Number(page -1)] : [];
}

module.exports = articleModel;
