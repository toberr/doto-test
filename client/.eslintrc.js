// https://eslint.org/docs/user-guide/configuring

module.exports = {
  'parser': 'vue-eslint-parser',
  'parserOptions': {
    'parser': 'babel-eslint',
    'sourceType': 'module'
  },
  env: {
    browser: true,
  },
  extends: [
    // https://github.com/vuejs/eslint-plugin-vue#priority-a-essential-error-prevention
    // consider switching to `plugin:vue/strongly-recommended` or `plugin:vue/recommended` for stricter rules.
    'eslint:recommended',
    'plugin:vue/essential',
  ],
  // required to lint *.vue files
  plugins: [
    'vue',
    'import'
  ],
  // check if imports actually resolve
  settings: {
    'import/resolver': 'webpack'
  },
  // add your custom rules here
  rules: {
    // allow debugger during development
    'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
    'indent': [1, 2],
    //'no-console': 'off',
    'linebreak-style': ['error', 'unix'],
    'comma-dangle': 'off',
    'no-trailing-spaces': 'off',
    'no-unused-vars': ['error', {'varsIgnorePattern': '(^log|^Vue|^__dirname)', 'args': 'none'}],
    'import/no-unresolved': ['error', {'commonjs': true}]
  }
}
