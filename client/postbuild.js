const fs = require('fs-extra');
const path = require('path');
const globby = require('globby');

// copy files
let paths = globby.sync([
  path.join('./src/assets/**/*'),
  // path.resolve('./src/template/*.html')
]).filter(x => !fs.lstatSync(x).isDirectory());

paths.forEach(function (x) {
  console.log(x, path.join(x.replace('src/', 'dist/')));
  fs.copySync(x, path.join(x.replace('src/', 'dist/')));
});
