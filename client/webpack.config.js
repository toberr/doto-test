/* eslint-env node */
const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const VueLoaderPlugin = require('vue-loader/lib/plugin');

const ENV = process.env.NODE_ENV || 'development';
const PROD = ENV === 'production';
console.log('client __dirname', __dirname);

module.exports = {
  context: path.resolve(__dirname, 'src'),
  entry: {
    main: './main.js',
    vendor: [
      'vue'
    ],
    polyfill:[
      'core-js/features/promise',
      'core-js/features/object/assign',
      'core-js/features/object/keys',
      'core-js/features/object/values',
      'core-js/features/array/find',
    ]
  },
  output: {
    path: path.resolve(__dirname, './dist'),
    publicPath: '/',
    filename: 'assets/js/bundle.[name].js',
    chunkFilename: 'assets/js/chunk.[name].js',
  },
  module: {
    rules: [
      {
        test: /\.vue$/,
        use: 'vue-loader'
      },
      {
        test: /\.s?css$/,
        use: [
          {
            loader: PROD ?
              MiniCssExtractPlugin.loader :
              'vue-style-loader',
            options: { sourceMap: true }
          },
          {
            loader: 'css-loader',
            options: { sourceMap: true }
          },
          {
            loader: 'postcss-loader',
            options: { sourceMap: true }
          },
          {
            loader: 'sass-loader',
            options: { sourceMap: true }
          }
        ]
      },
      {
        test: /\.js$/,
        loader: 'babel-loader',
        exclude: file => (
          /node_modules/.test(file) &&
          !/\.vue\.js/.test(file)
        )
      },
      {
        test: /\.(png|jpg|gif|svg|woff|ttf|eot)$/,
        loader: 'file-loader',
        options: {
          name: '[path][name].[ext]',
          context: './src',
        }
      }
    ]
  },
  optimization: {
    /* splitChunks: {
      chunks: 'all'
    } */
  },
  plugins: [
    new VueLoaderPlugin(),
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify(ENV),
    }),
    new HtmlWebpackPlugin({
      inject: true,
      filename: 'index.html',
      template: './template/index.html'
      //path.resolve(__dirname, 'src', 'template', 'index.html')
    }),
    new MiniCssExtractPlugin({
      filename: "assets/css/[name].css",
      chunkFilename: "assets/css/chunk.[name].css"
    })
  ],
  resolve: {
    extensions: ['.vue', '.js', '.json', '.scss'],
    modules: [
      path.resolve(__dirname, 'src'),
      path.resolve(__dirname, 'node_modules'),
      'node_modules',
    ],
    alias: {
      // only use it if you need the compiler
      'vue$': 'vue/dist/vue.esm.js'
    }
  },
  devServer: {
    port: process.env.PORT || 8080,
    host: '0.0.0.0',
    publicPath: '/',
    contentBase: [
      path.join(__dirname, 'src')
    ],
    watchContentBase: true,
    historyApiFallback: true,
    open: false,
    disableHostCheck: true,
    hot: true,
    writeToDisk: true,
    // liveReload: true,
    clientLogLevel: 'error',
  },
  performance: {
    hints: false
  },
  stats: { colors: true },
  devtool: 'source-map'
}

if (process.env.NODE_ENV === 'production') {
  module.exports.devtool = '#source-map'
  // http://vue-loader.vuejs.org/en/workflow/production.html
  module.exports.plugins = (module.exports.plugins || []).concat([
    new webpack.NamedModulesPlugin(),
    new webpack.optimize.ModuleConcatenationPlugin(),
    new webpack.LoaderOptionsPlugin({
      minimize: true
    })
  ])
}
