import Vue from 'vue';
import axios from 'axios';
import App from './components/App.vue';

let test = {
  log(x) {
    console.log(x);
  }
}

test.log(1);

Vue.prototype.$http = axios;

new Vue({
  el: '#app',
  render: h => h(App)
});
