function timeFormat(time) {
  for (let key in time) {
    if (time[key]) {
      return `${time[key]} ${key}s ago`;
    }
  }
}

function timeLeft(past) {
  past = past * 1000;
  return timeFormat(timeDiff(past));
}

export {
  timeFormat,
  timeLeft,
};
